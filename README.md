# Trade Guild

[![RimWorld](https://img.shields.io/badge/RimWorld-1.3-informational)](https://rimworldgame.com/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)

This [RimWorld](https://rimworldgame.com) mod adds a faction of interstellar traders to the game. It requires the [Royalty DLC](https://rimworldgame.com/royalty/) and the [Harmony mod](https://steamcommunity.com/workshop/filedetails/?id=2009463077). Using the [Royalty Framework mod](https://steamcommunity.com/workshop/filedetails/?id=2720308936) is recommended. Trade Guild adds a lot of extra content when used with supported mods; check the additional content section for details.

**Note:** The mod is currently in development. Favor costs are currently not balanced.

## Description

The Trade Guild is a loose association of interstellar merchant ships specialized in bringing the exotic and dangerous products gathered by rimworld inhabitants to civilized space. In return they offer commodities and goods that are difficult or even impossible to obtain in the rim.

This faction has no permanent settlements and can only be contacted through the comms console.

Each Guild ship type is specialized in certain wares. They offer their most common items at regular prices, but the most rare ones always come at a premium. They will also pay extra for rare rim resources they are interested on.

Although many consider these practices abusive, there are plenty of rimworld settlements who prefer to acquire resources that can immediately help in their survival instead of hoarding their wealth.

The most enterprising rimworlders take advantage of their rare local resources to turn a profit while trading with the Guild. By helping Guild members they will eventually gain enough Prestige to earn a permanent position in the Guild itself. A respected position will allow them to call in favours from the Guild from time to time.

## Features

### Prestige

Traders who frequently help other members of the guild and make economical contributions to it will increase their Prestige. After improving their position in the guild, a trader can use permits to ask the guild for favors such as getting transportation, resource drops or requesting specific types of guild ship.

The Prestige mechanic is the equivalent of the Empire's Honor.

### Position

Positions are similar to imperial titles, but the guild is not as rigid as the feudalist and archaic system of the empire. Guild titles cannot be inherited and do not require any ceremonies. Trade guild positions never cause a pawn to become conceited. The guild does not automatically grant psylink levels to its members, but psycaster guild ships usually have psylink neuroformers in stock.

Due to lack of modding support, positions are still called "titles" in-game. 

**Newcomer:** Requires 5 prestige points.

**Apprentice:** Requires 10 prestige points. Awards 1 permit point.

**Trader:** Requires 15 prestige points. Awards 1 permit point.

**Merchant:** Requires 20 prestige points. Awards 1 permit point.

**Tycoon:** Requires 25 prestige points. Awards 2 permit point.

**Magnate:** Requires 30 prestige points. Awards 2 permit point.

**Guildmaster:** This position cannot be achieved by the player.

### Permits

Trade Guild permits represent favors that can be requested by Guild members. They work in a similar way to imperial permits.

**Wood drop (Apprentice) [Cooldown 30, Prestige cost 3]:** Call for a drop of 400 wood.

**Steel drop (Apprentice) [Cooldown 30, Prestige cost 3]:** Call for a drop of 250 steel.

**Cloth drop (Apprentice) [Cooldown 30, Prestige cost 3]:** Call for a drop of 300 cloth.

**Silver drop (Trader) [Cooldown 30, Prestige cost 5]:** Call for a drop of 575 silver.

**Chemfuel drop (Trader) [Cooldown 30, Prestige cost 5]:** Call for a drop of 250 chemfuel.

**Transport shuttle (Trader) [Cooldown 30, Prestige cost 5]:** Call a shuttle for your own use. It will transport colonists, items, and animals on a one-way trip to anywhere you like within 70 world tiles. The shuttle can hold a total of 1000kg when loaded on a colony map.

**Orbital strike (Merchant) [Cooldown 30, Prestige cost 7]:** Call a single-impact orbital bomb strike at a target position.

**Orbital salvo (Tycoon) [Cooldown 40, Prestige cost 9]:** Call an extended salvo of orbital bomb strikes around a target position.

**Shell drop (Tycoon) [Cooldown 40, Prestige cost 9]:** Call for a drop of 25 high-explosive shells, 5 incendiary shells and 3 EMP shells.

**Siege drop (Magnate) [Cooldown 50, Prestige cost 11]:** Call for a drop of 450 steel, 12 components, 2 reinforced barrels and 10 explosive shells.

**Mech cluster targeter(Magnate) [Cooldown 50, Prestige cost 11]:** Drops a mechanoid combat cluster at the targeted point.

### Providing supplies

Keeping an interstellar commerce corporation running is an expensive venture. Local contributions are always appreciated, and giving supplies generously is a sure way to gain Prestige.

The most experienced member of the guild in the colony can contact the Trade Guild through the comms console and select the **Provide supplies** option. The trade guild supplies collector is an orbital trader which is always present, and accepts useful resources for interstellar travel in exchange for Prestige.

## Additional content

The mod enables extra content if any of the following DLCs or mods are also used.

* [Ideology DLC](https://rimworldgame.com/ideology/): Ideology preset for the guild.
* [Alpha Memes](https://steamcommunity.com/sharedfiles/filedetails/?id=2661356814): Expanded ideology preset for the guild.
* [Vanilla Ideology Expanded - Memes and Structures](https://steamcommunity.com/sharedfiles/filedetails/?id=2636329500): Expanded ideology preset for the guild.

## Contributions

This project encourages community involvement and contributions. Check the [CONTRIBUTING](CONTRIBUTING.md) file for details. Existing contributors can be checked in the [contributors list](https://gitlab.com/joseasoler/trade-guild/-/graphs/main).

## License

This project is licensed under the MIT license. Check the [LICENSE](LICENSE) file for details.

## Acknowledgements

Read the [ACKNOWLEDGEMENTS.md](ACKNOWLEDGEMENTS.md) file for details.
