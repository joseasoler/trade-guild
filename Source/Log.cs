using System;
using TradeGuild.Mod;
using Verse;

namespace TradeGuild
{
	/// <summary>
	/// Logs messages conditionally depending on mod settings and prepends a mod-specific prefix.
	/// </summary>
	public static class Log
	{
		private const string Prefix = "[Trade Guild] ";

		/// <summary>
		/// Lazily initialized reference to the settings of this mod.
		/// </summary>
		private static readonly Lazy<Settings> Settings =
			new Lazy<Settings>(LoadedModManager.GetMod<Mod.Mod>().GetSettings<Settings>);

		/// <summary>
		/// Logs orbital trader procedural generation messages. 
		/// </summary>
		/// <param name="text">Text to be logged.</param>
		public static void TraderGen(string text)
		{
			if (Settings.Value.ShowTradeGenLogs)
			{
				Verse.Log.Message(Prefix + text);
			}
		}
	}
}