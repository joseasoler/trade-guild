using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;

namespace TradeGuild.TraderGen
{
	/// <summary>
	/// Procedurally generated orbital trader ship.
	/// </summary>
	public class ProceduralTradeShip : TradeShip
	{
		/// <summary>
		/// Procedural generation template used to create the TraderKindDef.
		/// </summary>
		private TraderGenDef _genDef;
		
		/// <summary>
		/// Seed used to generate the TraderKindDef using the TraderGenDef.
		/// </summary>
		private int _genSeed;

		/// <summary>
		/// Obtain the Faction that the TradeShip will use.
		/// </summary>
		/// <param name="def">Procedural generation template.</param>
		/// <returns>Faction.</returns>
		private static Faction GetFaction(TraderGenDef def)
		{
			return Find.FactionManager.AllFactions.Where(faction => faction.def == def.faction)
				.TryRandomElement(out var result)
				? result
				: null;
		}

		/// <summary>
		/// Procedurally generates a new TraderKindDef, and registers it into DefDatabase.
		/// </summary>
		/// <param name="genDef">Trader generation preset to use.</param>
		/// <param name="genSeed">Seed to use.</param>
		/// <returns>Newly generated TraderKindDef.</returns>
		private static TraderKindDef GenerateTraderKindDef(in TraderGenDef genDef, int genSeed)
		{
			var newDefName = genDef.defName + '_' + genSeed;

			// If the def already exists, assume that it has previously been created. This can happen while loading without
			// quitting RimWorld; in that case the def may still be there.
			DefDatabase<TraderKindDef>.defsByName.TryGetValue(newDefName, out var def);
			if (def != null)
			{
				Log.TraderGen($"TraderKindDef {newDefName} exists, generation unnecessary.");
				return def;
			}

			def = new TraderKindDef
			{
				defName = newDefName,
				label = genDef.label,
				modExtensions = genDef.modExtensions,
				modContentPack = genDef.modContentPack,
				fileName = genDef.fileName,
				generated = true,
				orbital = genDef.orbital,
				requestable = genDef.requestable,
				hideThingsNotWillingToTrade = genDef.hideThingsNotWillingToTrade,
				tradeCurrency = genDef.tradeCurrency,
				faction = genDef.faction,
				permitRequiredForTrading = genDef.permitRequiredForTrading
			};

			// ToDo use rng to procedurally generate any required fields of the TraderKindDef.
			// var rng = new Random(genSeed);

			// ToDo Initialize stock generators from TraderGenDef data. Remove these placeholders.
			def.stockGenerators = new List<StockGenerator>
			{
				new StockGenerator_SingleDef
				{
					thingDef = ThingDefOf.Silver,
					countRange = new IntRange(100, 20000)
				},
				new StockGenerator_SingleDef
				{
					thingDef = ThingDefOf.ComponentSpacer,
					countRange = new IntRange(100, 20000)
				}
			};
	
			def.PostLoad();

			ShortHashGiver.GiveShortHash(def, def.GetType());
			DefDatabase<TraderKindDef>.Add(def);
			Log.TraderGen($"Generated new TraderKindDef {def.defName}.");

			return def;
		}

		public ProceduralTradeShip()
		{
		}

		public ProceduralTradeShip(TraderGenDef traderGenDef, int seed) : base(GenerateTraderKindDef(traderGenDef, seed),
			GetFaction(traderGenDef))
		{
			_genDef = traderGenDef;
			_genSeed = seed;
			
			// ToDo procedural generation of names.
			/*
			this.name = NameGenerator.GenerateName(RulePackDefOf.NamerTraderGeneral, (IEnumerable<string>) TradeShip.tmpExtantNames);
			if (faction != null) {
				this.name = string.Format("{0} {1} {2}", (object) this.name, (object) "OfLower".Translate(), (object) faction.Name);
			}
			*/
			
			// Unused. Reduce space taken in saved games instead.
			randomPriceFactorSeed = 0;

			Log.TraderGen($"Generated new trade ship {name}");
		}

		public override void ExposeData()
		{
			Scribe_Defs.Look(ref _genDef, "genDef");
			Scribe_Values.Look(ref _genSeed, "genSeed");
			if (Scribe.mode == LoadSaveMode.LoadingVars)
			{
				// Ensure that the TraderKindDef has been generated properly before loading the base class.
				GenerateTraderKindDef(_genDef, _genSeed);
			}
			base.ExposeData();
		}

		/// <summary>
		/// After the orbital trader departs, its procedurally generated TraderKindDef must be removed from the game.
		/// </summary>
		public override void Depart()
		{
			base.Depart();
			
			var defName = def.defName;

			DefDatabase<TraderKindDef>.Remove(def);
			ShortHashGiver.takenHashesPerDeftype[def.GetType()].Remove(def.shortHash);
			Log.TraderGen($"Removed previously generated TraderKindDef {defName} and trade ship {name}");
		}
	}
}