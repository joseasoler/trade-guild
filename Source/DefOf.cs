using RimWorld;
using Verse;

namespace TradeGuild
{
	[RimWorld.DefOf]
	public static class DefOf
	{

		[MayRequireRoyalty]
		public static ThingDef TG_MechClusterTargeting;

		[MayRequireRoyalty]
		public static TraderKindDef TG_SuppliesCollector;

		[MayRequireRoyalty]
		public static FactionDef TG_TradeGuild_Faction;

		static DefOf() => DefOfHelper.EnsureInitializedInCtor(typeof (DefOf));
	}
}