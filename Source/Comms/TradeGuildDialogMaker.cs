﻿using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;

namespace TradeGuild.Comms
{
	/// <summary>
	/// Defines new Comms Console dialog options for the Trade Guild.
	/// </summary>
	public static class TradeGuildDialogMaker
	{
		/// <summary>
		/// Sets the Comms Console dialog options for the Trade Guild.
		/// </summary>
		/// <param name="negotiator">Pawn contacting the Trade Guild.</param>
		/// <param name="faction">Trade Guild faction.</param>
		/// <returns></returns>
		public static DiaNode FactionDialogFor(Pawn negotiator, Faction faction)
		{
			var leader = faction.leader;
			var leaderName = faction.leader.Name.ToStringFull.Colorize(ColoredText.NameColor);
			var root = new DiaNode("TG_FactionGreeting"
				.Translate(leaderName, negotiator.LabelShort, negotiator.Named("NEGOTIATOR"), leader.Named("LEADER"))
				.AdjustedFor(leader));

			var map = negotiator.Map;
			if (map != null && map.IsPlayerHome)
			{
				// New option for providing supplies to the Trade Guild.
				root.options.Add(new DiaOption("TG_ProvideSupplies".Translate())
				{
					action = () => ProvideSupplies(negotiator),
					linkLateBind = () => FactionDialogFor(negotiator, faction)
				});

				if (negotiator.skills.GetSkill(SkillDefOf.Social).TotallyDisabled)
				{
					root.options.Last().Disable("WorkTypeDisablesOption".Translate(SkillDefOf.Social.label));
				}
				else if (!IsMostSenior(negotiator, faction))
				{
					// Only the most experienced Trade Guild member in the faction can provide supplies.
					root.options.Last().Disable("TG_OnlyMostExperiencedTrader".Translate());
				}

				// Existing comms console options. Disabled unconditionally because the Trade Guild has no settlements.
				root.options.Add(new DiaOption("RequestTrader".Translate(15)));
				root.options.Last().Disable("TG_FactionIsOffWorld".Translate());
				root.options.Add(new DiaOption("RequestMilitaryAid".Translate(25)));
				root.options.Last().Disable("TG_FactionIsOffWorld".Translate());
			}

			root.options.Add(new DiaOption("(" + "Disconnect".Translate() + ")")
			{
				resolveTree = true
			});

			return root;
		}

		/// <summary>
		/// New dialog action for providing the Guild with supplies and obtaining Prestige in return.
		/// </summary>
		/// <param name="negotiator">Pawn initiating the communication.</param>
		public static void ProvideSupplies(Pawn negotiator)
		{
			// Assumes that the called faction has def TG_TradeGuild_Faction.
			// A single supplies collector trader is stored per map using a new component. 
			var map = negotiator.Map;
			var suppliesCollectorManager = map.GetComponent<SuppliesCollectorManager>();
			if (suppliesCollectorManager == null)
			{
				suppliesCollectorManager = new SuppliesCollectorManager(map);
				map.components.Add(suppliesCollectorManager);
			}

			suppliesCollectorManager.SuppliesCollector.TryOpenComms(negotiator);
		}

		/// <summary>
		/// Checks if the provided seniority is the maximum value for colonists in the provided list of pawns.
		/// </summary>
		/// <param name="pawns">List of pawns</param>
		/// <param name="faction">Trade Guild faction</param>
		/// <param name="seniority">Maximum seniority value</param>
		/// <returns>True if no free colonist has a seniority larger than the specified value.</returns>
		public static bool IsMaxSeniority(IEnumerable<Pawn> pawns, Faction faction, int seniority)
		{
			return pawns.All(pawn => !pawn.IsFreeColonist || pawn.GetCurrentTitleSeniorityIn(faction) <= seniority);
		}

		/// <summary>
		/// Checks if the negotiator has the highest Trade Guild title in the player faction.
		/// </summary>
		/// <param name="negotiator">Pawn contacting the Trade Guild.</param>
		/// <param name="faction">Trade Guild faction.</param>
		/// <returns>True if the negotiator has the largest Trade Guild seniority of the entire player faction.</returns>
		public static bool IsMostSenior(Pawn negotiator, Faction faction)
		{
			var seniority = negotiator.GetCurrentTitleSeniorityIn(faction);
			if (Enumerable.Any(Find.Maps, map => !IsMaxSeniority(map.mapPawns.pawnsSpawned, faction, seniority)))
			{
				return false;
			}

			if (Enumerable.Any(Find.WorldObjects.Caravans,
				    caravan => !IsMaxSeniority(caravan.PawnsListForReading, faction, seniority)))
			{
				return false;
			}

			return Find.WorldObjects.TravelingTransportPods.All(travelingTransportPod =>
				IsMaxSeniority(travelingTransportPod.Pawns, faction, seniority));
		}
	}
}