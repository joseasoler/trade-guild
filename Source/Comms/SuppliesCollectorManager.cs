﻿using Verse;

namespace TradeGuild.Comms
{
	/// <summary>
	/// Map component that creates and stores a single Trade Guild supplies collector orbital ship.
	/// </summary>
	public class SuppliesCollectorManager : MapComponent
	{
		public SuppliesCollectorShip SuppliesCollector;

		/// <summary>
		/// Initialization of the SuppliesCollectorManager instance.
		/// </summary>
		/// <param name="map">Player's map</param>
		public SuppliesCollectorManager(Map map) : base(map)
		{
			var faction = Find.FactionManager.FirstFactionOfDef(DefOf.TG_TradeGuild_Faction);
			SuppliesCollector = new SuppliesCollectorShip(DefOf.TG_SuppliesCollector, faction)
			{
				name = "TG_SuppliesCollector".Translate(),
				// SuppliesCollector is not managed by PassingShipManager, but PassingShip instances use it to access the map.
				passingShipManager = map.passingShipManager
			};
		}

		/// <summary>
		/// Save/load code.
		/// </summary>
		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Deep.Look(ref SuppliesCollector, "SuppliesCollector");

			if (Scribe.mode == LoadSaveMode.LoadingVars)
			{
				// SuppliesCollector is not managed by PassingShipManager, but PassingShip instances use it to access the map.
				SuppliesCollector.passingShipManager = map.passingShipManager;
			}
		}
	}
}