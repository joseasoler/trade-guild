﻿using RimWorld;

namespace TradeGuild.Comms
{
	/// <summary>
	/// Special orbital trade ship that collects supplies for the Trade Guild. It never leaves range.
	/// </summary>
	public class SuppliesCollectorShip : TradeShip
	{
		public SuppliesCollectorShip()
		{
		}

		public SuppliesCollectorShip(TraderKindDef def, Faction faction = null)
			: base(def, faction)
		{
		}

		/// <summary>
		/// Tick method for passing ships. An empty override prevents it from departing.
		/// </summary>
		public override void PassingShipTick()
		{
		}
	}
}