﻿using RimWorld;
using Verse;

namespace TradeGuild.Permits
{
	public class MechClusterTargeting : OrbitalStrike
	{
		private bool _firstTick = true;

		public override void SpawnSetup(Map map, bool respawningAfterReload)
		{
			duration = 120;
			base.SpawnSetup(map, respawningAfterReload);
		}

		public override void Tick()
		{
			if (_firstTick)
			{
				StartStrike();
				_firstTick = false;
			}

			if (TicksPassed >= duration)
			{
				var sketch = MechClusterGenerator.GenerateClusterSketch(1500f, Map, true, true);
				MechClusterUtility.SpawnCluster(Position, Map, sketch);
			}

			base.Tick();
		}
	}
}