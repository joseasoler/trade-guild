﻿using System;
using System.Collections.Generic;
using RimWorld;
using UnityEngine;
using Verse;

namespace TradeGuild.Permits
{
	public class RoyalTitlePermitWorker_MechClusterTargeter : RoyalTitlePermitWorker_Targeted
	{
		private Faction _faction;

		public override bool ValidateTarget(LocalTargetInfo target, bool showMessages = true)
		{
			if (CanHitTarget(target)) return true;

			if (target.IsValid && showMessages)
			{
				Messages.Message(def.LabelCap + ": " + "AbilityCannotHitTarget".Translate(), MessageTypeDefOf.RejectInput);
			}

			return false;
		}

		public override void DrawHighlight(LocalTargetInfo target)
		{
			GenDraw.DrawRadiusRing(caller.Position, def.royalAid.targetingRange, Color.white);
			if (target.IsValid)
			{
				GenDraw.DrawTargetHighlight(target);
			}
		}

		public override void OrderForceTarget(LocalTargetInfo target)
		{
			CallTargeter(target.Cell);
		}

		public override IEnumerable<FloatMenuOption> GetRoyalAidOptions(Map paramMap, Pawn pawn, Faction faction)
		{
			if (faction.HostileTo(Faction.OfPlayer))
			{
				yield return new FloatMenuOption(
					def.LabelCap + ": " + "CommandCallRoyalAidFactionHostile".Translate(faction.Named("FACTION")), null);
				yield break;
			}

			if (Faction.OfMechanoids == null)
			{
				yield return new FloatMenuOption(
					def.LabelCap + ": " + "MessageNoFactionForVerbMechCluster".Translate(), null);
				yield break;
			}

			string description = def.LabelCap + ": ";
			Action action = null;
			if (FillAidOption(pawn, faction, ref description, out var paramFree))
			{
				action = delegate { BeginCallTargeter(pawn, faction, paramMap, paramFree); };
			}

			yield return new FloatMenuOption(description, action, faction.def.FactionIcon, faction.Color);
		}

		private void BeginCallTargeter(Pawn paramCaller, Faction faction, Map paramMap, bool paramFree)
		{
			targetingParameters = new TargetingParameters();
			targetingParameters.canTargetLocations = true;
			targetingParameters.canTargetSelf = true;
			targetingParameters.canTargetFires = true;
			targetingParameters.canTargetItems = true;
			caller = paramCaller;
			map = paramMap;
			free = paramFree;
			_faction = faction;
			targetingParameters.validator = delegate(TargetInfo target)
			{
				var targetingRange = def.royalAid.targetingRange;
				if (targetingRange > 0f &&
					target.Cell.DistanceTo(paramCaller.Position) > targetingRange)
				{
					return false;
				}

				return !target.Cell.Fogged(paramMap);
			};
			Find.Targeter.BeginTargeting(this);
		}

		private void CallTargeter(IntVec3 targetCell)
		{
			GenSpawn.Spawn(DefOf.TG_MechClusterTargeting, targetCell, map);

			caller.royalty.GetPermit(def, _faction).Notify_Used();
			if (!free)
			{
				caller.royalty.TryRemoveFavor(_faction, def.royalAid.favorCost);
			}
		}
	}
}