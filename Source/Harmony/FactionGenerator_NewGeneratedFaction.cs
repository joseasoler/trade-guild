using System.Collections.Generic;
using HarmonyLib;
using RimWorld;
using RimWorld.Planet;
using Verse;

namespace TradeGuild.Harmony
{
	/// <summary>
	/// FactionGenerator.NewGeneratedFaction always creates one settlement for each non-hidden non-player faction even if
	/// settlementGenerationWeight is set to zero. To avoid transpiling, this settlement is removed for the Trade Guild
	/// in a postfix instead.
	/// </summary>
	[HarmonyPatch(typeof(FactionGenerator), "NewGeneratedFaction")]
	public class FactionGenerator_NewGeneratedFaction
	{
		[HarmonyPostfix]
		static void Postfix(FactionGeneratorParms parms)
		{
			// This code should only be executed for the Trade Guild faction.
			var factionDef = parms.factionDef;
			if (factionDef != DefOf.TG_TradeGuild_Faction)
			{
				return;
			}

			// Destroy all settlements of the specified faction.
			IEnumerable<Settlement> settlements =
				Find.WorldObjects.Settlements.FindAll(
					settlement => settlement.Faction.def == factionDef);
			foreach (var settlement in settlements)
			{
				settlement.Destroy();
			}
		}
	}
}