﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using TradeGuild.Comms;
using Verse;

namespace TradeGuild.Harmony
{
	/// <summary>
	/// Sets the Comms Console dialog options for the Trade Guild.
	/// </summary>
	[HarmonyPatch(typeof(FactionDialogMaker), "FactionDialogFor")]
	public class FactionDialogMaker_FactionDialogFor
	{
		[HarmonyPrefix]
		static bool Prefix(ref DiaNode __result, Pawn negotiator, Faction faction)
		{
			// 
			if (faction.def != DefOf.TG_TradeGuild_Faction) return true;
			__result = TradeGuildDialogMaker.FactionDialogFor(negotiator, faction);
			return false;
		}
	}
}