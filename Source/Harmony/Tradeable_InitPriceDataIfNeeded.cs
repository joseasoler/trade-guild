﻿using HarmonyLib;
using RimWorld;

namespace TradeGuild.Harmony
{
	/// <summary>
	/// Provides a default RoyalFavorValue to all items when supplying items to the Trade Guild.
	/// </summary>
	[HarmonyPatch(typeof(Tradeable), "InitPriceDataIfNeeded")]
	public class Tradeable_InitPriceDataIfNeeded
	{
		public const float MarketValueToFavor = 0.001f;

		/// <summary>
		/// Provides a default RoyalFavorValue to tradeable items.
		/// </summary>
		/// <param name="__instance">Tradeable item to patch.</param>
		[HarmonyPostfix]
		static void Postfix(Tradeable __instance)
		{
			if (TradeSession.TradeCurrency != TradeCurrency.Favor || TradeSession.trader.Faction == null ||
			    TradeSession.trader.Faction.def != DefOf.TG_TradeGuild_Faction)
			{
				return;
			}

			var pricePlayerSell = __instance.GetType().GetField(
				"pricePlayerSell", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
			pricePlayerSell?.SetValue(__instance,
				__instance.AnyThing.GetStatValue(StatDefOf.MarketValue) * MarketValueToFavor);
		}
	}
}