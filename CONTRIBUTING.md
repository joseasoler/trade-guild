# Contributing

Thank you for being interested on contributing to this mod! This project and all contributions to it must follow the [Contributor Covenant Code of Conduct](CODE_OF_CONDUCT.md) and the [Rimworld End-User License Agreement](https://store.steampowered.com/eula/294100_eula_1).

## Questions and bug reports

You can use the [issue tracker](https://gitlab.com/joseasoler/trade-guild/-/issues) to ask questions and report bugs but do not forget to search (including closed issues) to see if your entry has been posted before.

## Contributions

The project and its contributions are managed using the [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).

Before submitting a merge request for a feature, create an issue on the [tracker](https://gitlab.com/joseasoler/trade-guild/-/issues) to allow for discussing and refining the idea prior to its implementation.
